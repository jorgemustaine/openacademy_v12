import os
import logging
from odoo import models, api, exceptions, _
EXPORTS_DIR = os.environ['HOME']

_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):
    """inherit from product product to generate a server log to file adding
    info related to quantity product stock"""
    _inherit = 'product.product'

    @api.model
    def export_stock_level(self, stock_location, e_dir=None):
        if not e_dir:
            e_dir = EXPORTS_DIR
        _logger.info('export stock level for %s', stock_location.name)
        products = self.with_context(location=stock_location.id).search([])
        products = products.filtered('qty_available')
        _logger.debug('%d products in the location', len(products))
        fname = os.path.join(e_dir, 'stock_level.txt')
        try:
            with open(fname, 'w') as fobj:
                for prod in products:
                    fobj.write('%s \t %f\n' % (prod.name, prod.qty_available))

        except IOError:
            _logger.exception('Error while writing to %s in %s',
                              'stock_level.txt', e_dir)
            raise exceptions.UserError(_('unable to save file'))
