from odoo import models, api


class ResCompany(models.Model):
    """extend the functionality of model that register companies"""

    _inherit = 'res.company'

    @api.multi
    def update_phone_number(self, new_number):
        """rewrite the phone number of company"""
        self.ensure_one()
        company_as_superuser = self.sudo()
        company_as_superuser.phone = new_number
