import os
from datetime import timedelta

from odoo import models, fields, api, _, exceptions
from odoo.addons import decimal_precision as dp
from odoo.fields import Date as fDate


class BaseArchive(models.AbstractModel):
    _name = 'base.archive'
    active = fields.Boolean(default=True,
                            help='boolean describe if the record is available')

    def do_archive(self):
        for record in self:
            record.active = not record.active


class LibraryBook(models.Model):
    _name = 'library.book'
    _inherit = ['base.archive']
    _description = 'Library Book'
    _order = 'date_release desc, name'
    _rec_name = 'short_name'

    #  Basic type fields
    name = fields.Char('Title', required=True, help='add book name')
    manager_remarks = fields.Text('Manager Remarks')
    short_name = fields.Char(string='Short Title',
                             size=100, translate=False, required=True,
                             help='alias to book')
    notes = fields.Text('Internal Notes', help='comment related to book')
    state = fields.Selection(
        [('draft', 'Not Available'),
         ('available', 'Available'),
         ('lost', 'Lost')],
        'State', help='describe availability of the book')
    description = fields.Html(string='Description',
                              sanitize=True,
                              strip_style=False,
                              translate=False,
                              help='book description')
    cover = fields.Binary('Book Cover', help='picture of the cover')
    out_of_print = fields.Boolean('Out of Print?',
                                  help='specifies if it is out of print')
    #  Relational fields
    authors_ids = fields.Many2many('res.partner', string='Authors')

    #  Date fields
    date_release = fields.Date('Release Date',
                               help='date of release to the public')
    date_updated = fields.Datetime('Last Update',
                                   help='date for last update')
    pages = fields.Integer(string='Number of Pages',
                           default=0,
                           help='Total book page count',
                           groups='base.group_user',
                           states={'lost': [('readonly', True)]},
                           copy=True, index=False, readonly=False,
                           required=False,
                           company_dependent=False,)
    #   Float fields
    reader_rating = fields.Float(
        'Reader Average Rating',
        digits=(14, 4),   # Optional precision (total, decimals)
        help='book score'
    )
    cost_price = fields.Float('Book Cost', dp.get_precision('Book Price'),
                              help='Sale price')
    age_days = fields.Float(
        string='Days Since Release',
        compute='_compute_age',
        inverse='_inverse_age',
        search='_search_age',
        store=False,
        compute_sudo=False,
        help='Book age',
    )

    #  Monetary Fields
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  help='Valuation currency')
    retail_price = fields.Monetary(
        'Retail Price',
        #  optional: currency_field='currency_id',
        help='retailer price',
    )

    #  Relational fields
    author_ids = fields.Many2many(
        'res.partner',
        string='Authors',
        help='writers of the work'
    )
    publisher_id = fields.Many2one(
        'res.partner', string='Publisher',
        #  optional:
        ondelete='set null',
        help='Editorial'
    )
    publisher_city = fields.Char(
        'Publisher City',
        related='publisher_id.city',
        readonly=True,
        help='publishing city',
    )
    category_id = fields.Many2one('library.book.category', 'Category',
                                  help='classification according to the theme '
                                       'of the book')

    #  Reference field
    ref_doc_id = fields.Reference(
        selection='_referencable_models',
        string='Reference Document',
        help='book reference',
    )

    @api.multi
    def name_get(self):
        """refactoring the search to m2o fields, this method is used by the
        orm to define how the data of an m2o relationship are obtained"""
        result = list()
        for book in self:
            authors = book.authors_ids.mapped('name')
            name = '%s (%s)' % (book.name, ', '.join(authors))
            result.append((book.id, name))
            return result

    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100,
                     name_get_uid=None):
        """improve the method with the ability to solve searches by isbn"""
        args = [] if args is None else args.copy()
        if not(name == '' and operator == 'ilike'):
            args += ['|', '|',
                     ('name', operator, name),
                     ('isbn', operator, name),
                     ('authors.name', operator, name),
                     ]
            return super(LibraryBook, self)._name_search(
                name='', args=args, operator='ilike', limit=limit,
                name_get_uid=name_get_uid)

    @api.multi
    def save(self, filename):
        """defines how a file should be saved and errors related to it """
        if '/' in filename or '\\' in filename:
            raise exceptions.UserError(_('Illegal filename %s') % filename)
        path = os.path.join('/opt/exports', filename)
        try:
            with open(path, 'w') as fobj:
                for record in self:
                    fobj.write(record.data)
                    fobj.write('\n')
        except (IOError, OSError) as exc:
            message = _('Unable to save file: %s') % exc
            raise exceptions.UserError(message)

    #   Add constrains
    _sql_constraints = [
        ('name_uniq',
         'UNIQUE (name)',
         'Book title must be unique.')
    ]

    @api.model
    def create(self, values):
        """overwrite create method this method define how the records are
        create in the db"""
        if not self.user_has_groups('library.group_library_manager'):
            if 'manager_remarks' in values:
                raise exceptions.UserError(_(
                    'You are not allowed to modify '
                    'manager_remarks'
                ))
        return super(LibraryBook, self).create(values)

    @api.multi
    def write(self, values):
        """overwrite write method this method define how the records are
        update in the db"""
        if not self.user_has_groups('library.group_library_manager'):
            if 'manager_remarks' in values:
                raise exceptions.UserError(_(
                    'You are not allowed to modify '
                    'manager_remarks'
                ))
        return super(LibraryBook, self).write(values)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        """extend field get method"""
        fields_books = super(LibraryBook, self).fields_get(
            allfields=allfields, attributes=attributes)
        if not self.user_has_groups('library.group_library_manager'):
            if 'manager_remarks' in fields_books:
                fields_books['manager_remarks']['readonly'] = True

        return fields_books

    @api.model
    def _referencable_models(self):
        """searches for methods that can be referred"""
        models_obj = self.env['res.request.link'].search([])
        return [(x.object, x.name) for x in models_obj]

    #  Add helper method to check state transition is allowed
    @api.model
    def is_allowed_transition(self, old_state, new_state):
        """check that state is allowed"""
        allowed = [('draft', 'available'),
                   ('available', 'borrowed'),
                   ('borrowed', 'available'),
                   ('available', 'lost'),
                   ('borrowed', 'lost'),
                   ('lost', 'available')]
        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        """change the state of some books to a new passed as an argument"""
        for book in self:
            if book in self:
                if book.is_allowed_transition(book.state, new_state):
                    book.state = new_state
                else:
                    continue

    @api.constrains('date_release')
    def _check_releases_date(self):
        """check that date is not in the future"""
        for record in self:
            if (
                record.date_release and (
                    record.date_release > fields.Date.today())):
                raise models.ValidationError(_(
                    'Release date must be in the past'))

    @api.depends('date_release')
    def _compute_age(self):
        """compute the time from release book"""
        today = fDate.from_string(fDate.today())
        for book in self.filtered('date_release'):
            delta = (today - fDate.from_string(book.date_release))
            book.age_days = delta.days

    def _inverse_age(self):
        """inverse compute function"""
        today = fDate.from_string(fDate.context_today(self))
        for book in self.filtered('date_release'):
            day = today - timedelta(days=book.age_days)
            book.date_release = fDate.to_string(day)

    def _search_age(self, operator, value):
        """define the searching on age of books"""
        today = fDate.from_string(fDate.context_today(self))
        value_days = timedelta(days=value)
        value_date = fDate.to_string(today - value_days)
        #  convert the operator:
        #  book with age > value have a date < value_date
        operator_map = {
            '>': '<', '>=': '<=',
            '<': '>', '<=': '>=',
        }
        new_op = operator_map.get(operator, operator)
        return [('date_release', new_op, value_date)]

    @api.model
    def get_all_library_members(self):
        """returns the member set"""
        library_member_model = self.env['library.member']
        return library_member_model.search([])


class ResPartner(models.Model):
    """inherit model of base res partner"""
    _inherit = 'res.partner'
    _order = 'name'

    name = fields.Char('Name', required=True, help='Name of author')
    email = fields.Char('Email', help='Electronic mail of author')
    date = fields.Date('Date', help='date of birth of author')
    is_company = fields.Boolean('Is a company',
                                help="Check if the contact is a company, "
                                "otherwise it is a person"
                                )
    parent_id = fields.Many2one('res.partner', 'Related company',
                                help="refer to a company of contact or author")
    child_ids = fields.One2many('res.partner', 'parent_id', 'Contacts',
                                help="Contacts if partner is company")
    authored_book_ids = fields.Many2many(
        'library.book', string='Authored Books',
        relation='library_book_res_partner_rel',
        help='writers of the work'
    )
    count_books = fields.Integer(
        'Number of Authored Books',
        compute='_compute_count_books', help='total books'
    )
    published_book_ids = fields.One2many(
        'library.book', 'publisher_id', string='Published Books',
        help='publishers'
    )

    @api.model
    def add_contacts(self, partner, contacts):
        """update the contacts to partner"""
        partner.ensure_one()
        if contacts:
            partner.date = fields.Date.context_today(self)
            partner.child_ids |= contacts

    @api.model
    def find_partners_and_contacts(self, name):
        """seek the partners and return"""
        partner = self.env['res.partner']
        domain = ['|', '&',
                  ('is_company', '=', True),
                  ('name', 'like', name),
                  '&',
                  ('is_company', '=', False),
                  ('parent_id.name', 'like', name),
                  ]
        return partner.search(domain)

    @api.model
    def get_email_addresses(self, partner):
        """return electronic mail address"""
        return partner.mapped('child_ids.email')

    @api.model
    def get_companies(self, partners):
        """return companies of partners"""
        return partners.mapped('partner_id')

    @api.model
    def partners_with_email(self, partners):
        """get all partners with email"""
#        def predicate(partner):
#            if partner.email:
#                return True
#            return False
#
#        return partners.filter(predicate)
        return partners.filter(lambda p: p.email)

    @api.depends('authored_book_ids')
    def _compute_count_books(self):
        """Count the number of books per author"""
        for author in self:
            author.count_books = len(author.authored_book_ids)
