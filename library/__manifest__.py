# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    'name': 'library v11',
    'version': '11.0.1.0.0',
    'license': 'AGPL-3',
    'website': 'https://www.jorgescalona.github.io',
    'summary': 'repo with didactical purpose about odoo v12',
    'author': '@jorgemustaine',
    'depends': ['base', 'decimal_precision', 'product', 'stock'],
    'demo': ['demo/demo.xml'],
    'data': ['data/res_partner.xml',
             'views/library_book.xml',
             'security/groups.xml',
             'security/ir.model.access.csv',
             ],
    'application': True,
    'installable': True,
}
