from .common import TestResCompanyCommon


class ResCompanyTestCase(TestResCompanyCommon):
    """here define all env to unit test from modules"""

    def setUp(self):
        super(ResCompanyTestCase, self).setUp()

    def test_10_update_phone_of_company(self):
        """Test that number of company only update by superuser"""

        #  with user <> admin or superuser same phone I should not let modify
        #  records in res company model but we tested the use of sudo in the
        #  created method
        #  First chek number
        self.assertEqual(self.company.phone, '555')
        self.company.sudo(self.demo_user).update_phone_number('3333')
        #  after check that phone changed
        self.assertEqual(self.company.phone, '3333')
