from openerp.tests import common


class TestResCompanyCommon(common.TransactionCase):

    def setUp(self):
        super(TestResCompanyCommon, self).setUp()

        self.company = self.env.ref('library.company_test_00')
        self.demo_user = self.env.ref('base.user_demo')
        self.product_00 = self.env.ref('library.product_test_00')
        self.location_00 = self.env.ref('library.stock_library_location_00')
