from odoo.tests.common import TransactionCase


class LibraryTestCase(TransactionCase):
    """here define all env to unit test from modules"""

    def setUp(self):
        super(LibraryTestCase, self).setUp()
        demo_user = self.env.ref('base.user_demo')
        demo_user.groups_id |= self.env.ref('library.group_librarian')

        book_model = self.env['library.book'].sudo(demo_user)
        self.book = book_model.create(
            {'name': 'Test book',
             'short_name': 'TB',
             'state': 'draft'
             }
        )

    def test_00_change_draft_available_00(self):
        """test changing state from draft to available"""
        self.book.change_state('available')
        self.assertEqual(self.book.state, 'available')

    def test_01_change_available_draft_no_effect_01(self):
        """test forbidden state change from available to draft"""
        self.book.change_state('available')
        self.book.change_state('draft')
        self.assertEqual(self.book.state, 'available',
                         'the state cannot change from available to %s' %
                         self.book.state)
