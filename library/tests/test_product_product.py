import os
import re
#  from openerp import exceptions
from .common import TestResCompanyCommon


class ProductTestCase(TestResCompanyCommon):
    """here define all env to unit test from modules"""

    def setUp(self):
        super(ProductTestCase, self).setUp()
        self.EXPORTS_DIR = os.environ['HOME']
        self.env['stock.quant'].create(
            {'product_id': self.product_00.id,
             'location_id': self.location_00.id,
             'quantity': 30
             })

    def test_10_product_availability(self):
        """Test that check availability of product in location"""
        self.product_00.export_stock_level(self.location_00)
        list_dir = os.listdir(self.EXPORTS_DIR)
        #  Check that file is created
        indice = list_dir.index('stock_level.txt')
        self.assertEqual(list_dir[indice], 'stock_level.txt')
        fname = self.EXPORTS_DIR+'/'+list_dir[indice]
        with open(fname, 'rt') as myfile:
            content = myfile.read()
        #  Check qty of product in location
        self.assertEqual(int(re.findall(r'\d+', content.split()[4])[0]), 30)

        #  Eliminate file
        os.remove(fname)

        # TODO: create test tu logs

        #   set path var to restricted directory
        # e_dir = '/srv/exports'
        # self.assertIn('Error while writing to %s in %s', cm.output)
        #   Test UserError
        # with self.assertRaises(exceptions.UserError):
        #    self.product_00.export_stock_level(self.location_00, e_dir)
