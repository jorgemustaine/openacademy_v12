[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[![coverage report](https://gitlab.com/jorgemustaine/openacademy_v12/badges/11.0/coverage.svg)](https://gitlab.com/jorgemustaine/openacademy_v12/commits/11.0)

[![Coverage Status](https://coveralls.io/repos/gitlab/jorgemustaine/openacademy_v12/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/jorgemustaine/openacademy_v12?branch=HEAD)

[![codecov](https://codecov.io/gl/jorgemustaine/openacademy_v12/branch/11.0/graph/badge.svg)](https://codecov.io/gl/jorgemustaine/openacademy_v12)

# Opencademy Odoo v12

This project for educational purpose.

* @jorgemustaine jorgescalona escajorge@gmail.com 2019
